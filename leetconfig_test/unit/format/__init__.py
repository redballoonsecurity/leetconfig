from typing import Any, Dict, List, Optional, Sequence, Tuple

from leetconfig.entry import ConfigEntry
from leetconfig.entry_converter import ConfigEntryConverter
from leetconfig.format.abstract_format import ConfigFormat
from leetconfig.group import ConfigGroup
from leetconfig.namespace import ConfigNamespace


class ConfigEntryTestCase(object):
    def __init__(
        self,
        test_name,  # type: str
        entry_name,  # type: Tuple[str, str]
        definition_flags,  # type: Dict[str, Any]
        expected_value,  # type: Any
    ):
        self.test_name = test_name
        self.entry_name = entry_name
        self.definition_flags = definition_flags
        self.expected_value = expected_value


class NullConfigEntryConverter(ConfigEntryConverter[Any]):
    def get_target_type(self):  # type: () -> str
        return "any"

    def deserialize(self, value):  # type: (Any) -> str
        return value

    def serialize(self, value):  # type: (Any) -> Any
        return value


class TestFormat:
    @staticmethod
    def create_short_name(name):
        # type: (str) -> str
        return "".join([p[0] for p in name.split("_")])

    @staticmethod
    def _create_config_definition(
        test_case,  # type: ConfigEntryTestCase
    ):
        # type: (...) -> ConfigEntry[Any]
        return ConfigEntry(
            test_case.entry_name[0],
            short_names=test_case.entry_name[1],
            parser=NullConfigEntryConverter(),
            help="Test config entry",
            **test_case.definition_flags
        )

    @staticmethod
    def _create_config_group(
        config_entries,  # type: List[ConfigEntry]
        nested_names,  # type: Sequence[Optional[str]]
    ):
        # type: (...) -> ConfigGroup
        leaf = ConfigGroup(entries=config_entries)
        for nested_name in reversed(nested_names):
            if nested_name is None:
                leaf = ConfigGroup(groups=[leaf])
            else:
                leaf = ConfigNamespace(
                    nested_name,
                    TestFormat.create_short_name(nested_name),
                    groups=[leaf],
                )
        return ConfigGroup(groups=[leaf])

    @staticmethod
    def validate_deserialization(
        config_format,  # type: ConfigFormat
        test_cases,  # type: Sequence[ConfigEntryTestCase]
        nested_names,  # type: List[Optional[str]]
    ):
        config_entries = [
            TestFormat._create_config_definition(test_case) for test_case in test_cases
        ]
        values = config_format.deserialize(
            TestFormat._create_config_group(config_entries, nested_names)
        )
        for test_case, config_entry in zip(test_cases, config_entries):
            value = values.get(config_entry._get_namespaced_id())
            assert value == test_case.expected_value

    @staticmethod
    def validate_serialization(
        config_format,  # type: ConfigFormat
        test_cases,  # type: Sequence[ConfigEntryTestCase]
        nested_names,  # type: Sequence[Optional[str]]
        expected_value,  # type: Any
    ):
        config_entries = []
        for test_case in test_cases:
            config_definition = TestFormat._create_config_definition(test_case)
            config_definition.set_value(test_case.expected_value)
            config_entries.append(config_definition)
        value = config_format.serialize(
            TestFormat._create_config_group(config_entries, nested_names)
        )
        assert value == expected_value
