from typing import Tuple

import pytest
from enum import Enum

from leetconfig.entry_converter import (
    EnumConfigEntryConverter,
    ConfigValueSerializationError,
    ConfigValueDeserializationError,
)


class MockEnum(Enum):
    VALUE_1 = "value1"
    VALUE_2 = "value2"


class MockOtherEnum(Enum):
    OTHER_VALUE_1 = "other_value1"


class TestEnumConfigEntryConverter(object):
    @pytest.mark.parametrize("enum", [MockEnum])
    def test_serialize(self, enum):
        converter = EnumConfigEntryConverter(enum)
        for enum_value in enum:
            serialized = converter.serialize(enum_value)
            assert serialized == enum_value.name

    def test_serialize_error(self):
        converter = EnumConfigEntryConverter(MockEnum)
        with pytest.raises(ConfigValueSerializationError):
            converter.serialize(MockOtherEnum.OTHER_VALUE_1)  # type: ignore

    @pytest.mark.parametrize(
        "test_values",
        [
            (MockEnum, MockEnum.VALUE_1.name),
            (MockEnum, MockEnum.VALUE_1.value),
        ],
    )
    def test_deserialize(self, test_values):
        # type: (Tuple[Enum, str]) -> None
        enum, raw_value = test_values
        self._test_deserialize(enum, raw_value)

    @pytest.mark.parametrize(
        "test_values",
        [
            (MockEnum, MockEnum),
            (MockOtherEnum, MockEnum.VALUE_1.name),
            (MockOtherEnum, MockEnum.VALUE_1.value),
        ],
    )
    def test_deserialize_error(self, test_values):
        # type: (Tuple[Enum, str]) -> None
        enum, raw_value = test_values
        with pytest.raises(ConfigValueDeserializationError):
            self._test_deserialize(enum, raw_value)

    @staticmethod
    def _test_deserialize(enum, raw_value):
        converter = EnumConfigEntryConverter(enum)
        enum_value = converter.deserialize(raw_value)
        assert enum_value in enum
