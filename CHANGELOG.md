# Changelog

`leetconfig` adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html), and this file is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## 0.1.0 - 2024-02-12
### Changed
- Remove the python < 3.8 restriction

### Fixed
- Update type annotations to resolve mypy errors and warnings
